"use strict";

const APIURL = "https://ron-swanson-quotes.herokuapp.com/v2/quotes";

document.addEventListener("DOMContentLoaded", setup, false);

function setup() {
    let btn = document.querySelector("#btn");
    btn.onclick = getQuote;
}

function getQuote(e) {
    e.preventDefault();
    var quote = document.querySelector("#quote");
    fetch(APIURL)
        .then (
            function(response) {
                if (response.status !== 200) {
                    quote.innerHTML = "There was an error grabbing quote :(, Try again";
                    return
                }

                response.json().then(function(data) {
                    quote.innerHTML = "\"" + data;
                })
            }
        )
}